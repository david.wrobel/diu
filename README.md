# Formation DIU - Lille

## Les différents blocs

La formation est structurée en bloc dont la description est donnée par :

|        Bloc 1         |    Bloc 2     |       Bloc 3       |       Bloc 4       |
| :-------------------: | :-----------: | :----------------: | :----------------: |
| Structures de données | Algorithmique | Système et réseaux | BDD et POO         |

On trouvera les différents TPs en suivant les liens suivants :

> * [Bloc 1](Blocs/Bloc 1/README.md)
> * [Bloc 2](Blocs/Bloc 1/README.md)
> * [Bloc 3](Blocs/Bloc 3/README.md)
> * [Bloc 4](Blocs/Bloc 4/README.md)
