# Formation DIU - Lille / Bloc 1

## Projet ADN

>* [Lien](01 - ADN/README.md) vers la page.

## Projet Jeu de la vie

> * [Lien](02 - Conway/README.md) vers la page.

## Projet Phrases-passe

> * [Lien](03 - Phrases-Passe/README.md) vers la page.

## Projet WaToR

> * [Lien](04 - WaToR/README.md) vers la page.

## Projet Courses des chicons

> * [Lien](04 - Chicons/README.md) vers la page.

