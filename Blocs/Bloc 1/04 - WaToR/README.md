# Simulation d'un système proie-prédateur

## Présentation de Wa-tor

...

## Travail à faire

### Les différents modules

+ Constantes du programme
+ Gestion de la mer
+ Gestion des poissons
+ Programme principal

Ces modules interagissent entre eux via le diagramme suivant :

```mermaid
graph LR
A(constantes.py) -->B(mer.py)
    B --> C(poissons.py)
    C --> D(main.py)
```

### Les structures de données

La mer M sera représentée par un tableau bi-dimensionnel. 
L'élément M[i][j] représentera la case située à la ligne i et à la colonne j.

![La mer](Images/mer.png)

Chacune des cases sera un dictionnaire. Pour le thon, on propose le dictionnaire ci-dessous.

- {'Type' : ID_THON, 'Gestation' : GESTATION_THON}

où ID_THON est un identifiant que l'on peut choisir égal à la chaîne de caractères 'Thon' et GESTATION_THON est une constante représentant la gestation du thon que l'on peut définir comme étant égal à 2.

1) Définir les dictionnaires représentant les cases vide et celles contenant un requin.

### Gestion de la mer

2) Écrire une fonction `creer_ocean` qui renvoie un tableau bi-dimensionnel de cases vides.
Les dimensions du tableau sont passées en paramètre.

```python
>>> creer_ocean(3, 2)
[[{'Type': 'Vide'}, {'Type': 'Vide'}], [{'Type': 'Vide'}, {'Type': 'Vide'}], [{'Type': 'Vide'}, {'Type': 'Vide'}]]
```

3) Écrire une fonction `dimensions_ocean` qui renvoie les dimensions de l'océan passé en paramètre.
```python
>>> dimensions_ocean(creer_ocean(10, 5))
(10, 5)
```

4) Écrire une fonction `type_case` qui renvoie le type de case située à des coordonnées passées en paramètre.
```python
>>> type_case(creer_ocean(5, 5), (2, 1))
'Vide'
```

5) Écrire une procédure `remplir_ocean` qui prend en paramètre un océan, une proportion de thon et une proportion de requin et qui remplit l'océan aléatoirement avec le bon nombre de thons et de requin.
On arrondira le nombre de thons et de requins à l'entier inférieur et on pourra utiliser la fonction randrange du module random pour obtenir un entier aléatoire.

6) Écrire une fonction `contenu_ocean` qui prend en paramètre un océan et qui retourne la 
proportion de thons et de requins.
```python
>>> M = creer_ocean(10, 5)
>>> remplir_ocean(M, 0.5, 0.2)
>>> contenu_ocean(M)
(0.5, 0.2)
```

7) Pour des raisons de simplification, on considère que la mer est torique (repliée sur elle-même).
![La mer](Images/tore.png)

De cette façon, chaque case possède quatre cases voisines.
Écrire une fonction `voisins_case` qui retourne les coordonnées de toutes les cases voisines à une case dont les coordonnées sont passées en paramètre.
```python
>>> w = creer_ocean(3, 5)
>>> voisins_case(w, (1, 2))
((0, 2), (1, 1), (1, 3), (2, 2))
>>> voisins_case(w, (0, 0))
((2, 0), (0, 4), (0, 1), (1, 0))
```

8) Écrire une fonction `voisins_suivant_type` qui retourne les coordonnées des cases voisines d'un certain type passé en paramètre.
```python
>>> voisins_suivant_type(M, (1, 2), ID_THON)
((0, 2), (1, 3))
```

### Gestion des poissons

9) Écrire un prédicat `gestation_poisson` qui met à jour la gestation du poisson situé aux coordonnées passées en paramètre. Ce prédicat retourne `True` si la gestation est passée par 0 et `False` sinon.

10) Écrire une procédure `comportement_thon` qui gère le comportement du thon situé aux coordonnées passées en paramètre.

11) Écrire une procédure `comportement_requin` qui gère le comportement du requin situé aux coordonnées passées en paramètre.

12) Écrire une procédure `comportement_poisson` qui gère le comportement du poisson situé aux coordonnées passées en paramètre.

### Programme principal

13) Écrire une procédure `simuler_une_etape` qui choisit une case aléatoirement dans la mer et qui applique le comportement correspondant.

14) Compléter le fichier pour qu'il simule un système proie-prédateur dans une mer aux dimensions 20x20 avec 30% de thons et 10% de requins. Afficher alors l'évolution de la proportion des thons et des requins.

![Évolution](Images/courbe.png)

