from mer import *

def gestation_poisson(ocean, coord):
    """
    Met à jour la gestation du poisson
    
    :param ocean: (list) liste de liste
    :param coord: (tuple) tuple représentation les coordonnées
    :return: (int) True si la gestation est passée par 0, False sinon
    """
    x, y = coord
    ocean[x][y]["Gestation"] -= 1
    
    if (ocean[x][y]["Gestation"] == 0):
        ocean[x][y]["Gestation"] = GESTATION_THON \
                                   if type_case(ocean, coord) == ID_THON \
                                   else GESTATION_REQUIN
        return True # On précise qu'il y a eu une naissance
    else:
        return False # Pas de naissance
    
def comportement_thon(ocean, coord):
    """
    Déplace un thon dans l'océan
    
    :param ocean: (list) liste de liste
    :param coord: (tuple) tuple de coordonnées
    :return: NoneType
    """
    voisins = voisins_suivant_type(ocean, coord, ID_VIDE)
    x, y = coord
        
    bGestation = gestation_poisson(ocean, coord)
    
    # S'il peut se déplacer
    if len(voisins) > 0:
        new_coord = choice(voisins)
        nx, ny = new_coord
        ocean[nx][ny] = ocean[x][y] #Déplacement
        if bGestation : # Si il y a reproduction
            ocean[x][y] = {'Type' : ID_THON, 'Gestation' : GESTATION_THON }
        else:
            ocean[x][y] = {'Type' : ID_VIDE}
            
def comportement_requin(ocean, coord):
    """
    Déplace un requin dans la ocean
    
    :param mer: (list) liste de liste
    :param coord: (tuple) tuple de coordonnées
    :return: NoneType
    """
    voisins_thons = voisins_suivant_type(ocean, coord, ID_THON)  
    x, y = coord
        
    bGestation = gestation_poisson(ocean, coord)
    bDeplacement = False
    
    ocean[x][y]["Energie"] -= 1
    
    if len(voisins_thons) > 0: #S'il a un voisin thon
        new_coord = choice(voisins_thons)
        nx, ny = new_coord
        bDeplacement = True
        ocean[x][y]["Energie"] = ENERGIE_REQUIN # On est sûr qu'il va manger un thon!!
    else:
        voisins_libres = voisins_suivant_type(ocean, coord, ID_VIDE)
        if len(voisins_libres) > 0: # S'il y a une case vide
            new_coord = choice(voisins_libres)
            nx, ny = new_coord        
            bDeplacement = True

    if (ocean[x][y]["Energie"] == 0) :
        ocean[x][y] = {'Type' : ID_VIDE}
    elif bDeplacement :
        ocean[nx][ny] = ocean[x][y]
        if bGestation :
            ocean[x][y] = {'Type' : ID_REQUIN, 'Gestation' : GESTATION_REQUIN,
                                   'Energie' : ENERGIE_REQUIN}
        else:
            ocean[x][y] = {'Type' : ID_VIDE}
            

def comportement_poisson(ocean, coord):
    if type_case(ocean, coord) == ID_THON:
        comportement_thon(ocean, coord)
    else:
        comportement_requin(ocean, coord)