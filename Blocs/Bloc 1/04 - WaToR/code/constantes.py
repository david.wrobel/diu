from random import randrange, choice
import pylab

""" LES IDENTIFIANTS """
ID_VIDE   = 'Vide'
ID_THON   = 'Thon'
ID_REQUIN = 'Requin'

""" CONSTANTES DU PROGRAMME """
GESTATION_THON   = 2
ENERGIE_REQUIN   = 3
GESTATION_REQUIN = 6