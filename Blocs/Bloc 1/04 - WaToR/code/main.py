from poissons import *

def simuler_une_etape(ocean):
    """
    Simule une étape.
    
    :param ocean: (list) une liste de liste
    :return 
    """
    li, co = dimensions_ocean(ocean)
    
    x = randrange(0, li)
    y = randrange(0, co)
    
    if (type_case(ocean, (x,y)) != ID_VIDE):
        comportement_poisson(ocean, (x,y))
        
if __name__ == "__main__":
    M = creer_ocean(20, 20)
    remplir_ocean(M, 0.3, 0.1)

    thons = [0.3]
    requins = [0.1]

    for i in range(100000):
        simuler_une_etape(M)
        t, r = contenu_ocean(M)
        thons.append(t)
        requins.append(r)
        
    pylab.plot(range(len(thons)), thons, '--')
    pylab.plot(range(len(requins)), requins, '--')
    pylab.legend(('Thons','Requins'))
    pylab.title("Évolution des populations")
    pylab.grid(True)
    pylab.show()