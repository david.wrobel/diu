from constantes import *
    
def creer_ocean(a, b):
    """
    Renvoie un tableau bi-dimensionnel de la forme a*b représentant l'océan vide
    
    :param a: (int) un entier pour le nombre de lignes
    :param b: (int) un entier pour le nombre de colonnes
    :return: (list) une liste de liste de dictionnaires
    """
    ocean = []
    for li in range(a):
        ligne = []
        for co in range(b):
            ligne.append({'Type' : ID_VIDE })
        ocean.append(ligne)
        
    return ocean

def dimensions_ocean(ocean):
    """
    Renvoie un tuple contenant les dimensions de l'océan
    
    :param ocean: (list) une liste de liste
    :return: (tuple) (a,b) avec a le nombre de lignes et b le nombre de colonnes
    
    >>> dimensions_ocean(creer_ocean(10, 25))
    (10, 25)
    """    
    return (len(ocean), len(ocean[0]))

def type_case(ocean, coord):
    """
    Renvoie le type de case.
    
    :param ocean: (list) une liste de liste
    :param coord: (tuple) coordonnées de la case
    :return: (identifiant) identifiant de la case
    """
    x, y = coord
    return ocean[x][y]["Type"]

def remplir_ocean(ocean, prop_thons, prop_requins):
    """
    Place aléatoirement dans l'océan le bon nombre de thons et de requins.
    
    :param ocean: (list) une liste de liste
    :param nb_thons: (int) proportion de thons à placer
    :param nb_requins: (int) proportion de requins à placer
    :return: NoneType
    :effets de bord: l'océan est modifié
    """
    li, co = dimensions_ocean(ocean)
    
    nb_thons = int(li*co*prop_thons)
    nb_requins = int(li*co*prop_requins)
    
    # Suivant le nombre de poissons à placer
    for k in range(nb_thons + nb_requins):
        bPlace = False
        while (not(bPlace)): # Tant que l'on a pas pu le placer
            x = randrange(0, li) # Choisir une ligne aléatoire
            y = randrange(0, co) # Choisir une colonne aléatoire
            if type_case(ocean, (x,y)) == ID_VIDE:
                if (k < nb_thons):
                    ocean[x][y] = {'Type' : ID_THON, 'Gestation' : GESTATION_THON }
                else:
                    ocean[x][y] = {'Type' : ID_REQUIN, 'Gestation' : GESTATION_REQUIN,
                                   'Energie' : ENERGIE_REQUIN}
                bPlace = True
                
def voisins_case(ocean, coord):
    """
    Renvoie un tuple consituté des coordonnées de tous les voisins
    de la case située à la ligne i et à la colonne j
    
    :param mer: (list) une liste de liste représentant la mer
    :param coord: (tuple) coordonnées de la case
    :return: (tuple) un tuple de tuple
    
    >>> w = creer_ocean(3, 5)
    >>> voisins_case(w, (1, 2))
    ((0, 2), (1, 1), (1, 3), (2, 2))
    >>> voisins_case(w, (0, 0))
    ((2, 0), (0, 4), (0, 1), (1, 0))
    """
    i, j = coord
    a, b = dimensions_ocean(ocean)
    
    vhaut   = ((i-1)%a, j)
    vgauche = (i, (j-1)%b)
    vdroit  = (i, (j+1)%b)
    vbas    = ((i+1)%a, j)
    
    return (vhaut, vgauche, vdroit, vbas)


def voisins_suivant_type(ocean, coord, t):
    """
    Renvoie les coordonnées des cases voisines qui contiennent le type désiré
    
    :param mer: (list) une liste de liste
    :param coord: (tuple) coordonnées de la case
    :param t: (identifiant) type recherché
    :return: (tuple) un tuple de tuple
    """
    ret = ()
    
    voisins = voisins_case(ocean, coord)
    
    for co in voisins:
        if type_case(ocean, co) == t:
            ret += (co,)
                
    return ret

def contenu_ocean(ocean):
    """
    Renvoie la fréquence des thons et des requins dans l'océan
    
    :param ocean: (list) une liste de liste
    :return: (tuple) fréquence des thons, fréquence des requins
    """
    lignes, colonnes = dimensions_ocean(ocean)
    dim = lignes*colonnes
    
    nb_thons = 0
    nb_requins = 0
    
    for x in range(lignes):
        for y in range(colonnes):
            t = type_case(ocean, (x,y))
            if t == ID_THON:
                nb_thons += 1
            elif t == ID_REQUIN:
                nb_requins += 1
                
    return nb_thons/dim, nb_requins/dim