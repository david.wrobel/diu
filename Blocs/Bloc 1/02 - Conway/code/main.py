def creer_grille(a, b):
    """
        Renvoie une liste contenant b listes contenant a zéros.
        Entrée :
            - a : entier représentant la largeur du rectangle
            - b : entier représentant la hauteur du rectangle
        Sortie :
            - Liste représentant le rectangle.
    """
    L = []
    
    for j in range(b):
        ligne = []
        for i in range(a):
            ligne += [0]
        L += [ligne]
        
    return L

def hauteur_grille(L):
    """
        Renvoie la hauteur d'une grille
    """
    return len(L)

def largeur_grille(L):
    """
        Renvoie la largeur d'une grille
    """
    return len(L[0])

from random import random

def creer_grille_aleatoire(a, b, p):
    """
        Remplit aléatoirement une grille
    """
    L = creer_grille(a,b)
    
    for j in range(b):
        for i in range(a):
            if random()<p:
                L[j][i] = 1 
            
    return L

def voisins_case(L, i, j):
    a = largeur_grille(L)-1
    b = hauteur_grille(L)-1
    
    if 0 < i < a: #Pas sur la première ni sur la dernière colonne
        if 0 < j < b: #Pas sur la première ni sur la dernière ligne
            V = L[j-1][i-1:i+2] + [L[j][i-1], L[j][i+1]] + L[j+1][i-1:i+2]
        elif j == 0: #Sur la première ligne
            V = [L[j][i-1], L[j][i+1]] + L[j+1][i-1:i+2]
        elif j == b: #Sur la dernière ligne
            V = L[j-1][i-1:i+2] + [L[j][i-1], L[j][i+1]]
    elif i == 0: #Sur la première colonne
        if 0 < j < b: #Pas sur la première ni sur la dernière ligne
            V = L[j-1][i:i+2] + [L[j][i+1]] + L[j+1][i:i+2]
        elif j == 0: #Sur la première ligne
            V = [L[j][i+1]] + L[j+1][i:i+2]
        elif j == b: #Sur la dernière ligne
            V = L[j-1][i:i+2] + [L[j][i+1]]
    elif i == a: #Sur la dernière colonne
        if 0 < j < b: #Pas sur la première ni sur la dernière ligne
            V = L[j-1][i-1:i+1] + [L[j][i-1]] + L[j+1][i-1:i+1]
        elif j == 0: #Sur la première ligne
            V = [L[j][i-1]] + L[j+1][i-1:i+1]
        elif j == b: #Sur la dernière ligne
            V = L[j-1][i-1:i+1] + [L[j][i-1]]
    
    return V
        
def nb_cellules_voisins(L, i, j):
    return sum(voisins_case(L, i, j))

def afficher_grille(L):
    for ligne in L:
        for c in ligne:
            if c == 0:
                print('_', end=' ')
            else:
                print('O', end= ' ')
        print('\n')
        
def generation_suivante(L):
    newL = [ligne.copy() for ligne in L]
    
    a = largeur_grille(L)
    b = hauteur_grille(L)
    
    for j in range(b):
        for i in range(a):
            v = nb_cellules_voisins(L, i, j)
            if v == 3:
                newL[j][i] = 1
            elif v < 2 or v > 3:
                newL[j][i] = 0
                
    return newL
    
from time import sleep

def evolution_n_generations(L, n):
    
    for k in range(n):
        afficher_grille(L)
        L = generation_suivante(L)
        sleep(1)