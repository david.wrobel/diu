from collections import namedtuple

Time = namedtuple('Time', ['hours', 'minutes', 'seconds'])

def create(hours, minutes, seconds):
    return Time(hours, minutes, seconds)

def time_in_second(temps):
    return int(temps.hours)*3600 + int(temps.minutes)*60 + int(temps.seconds)

def compare(temps1, temps2):
    t1 = time_in_second(temps1)
    t2 = time_in_second(temps2)
    
    return t1-t2
    
def to_string(temps):
    return '{:2}:{:2}:{:2}'.format(temps.hours.zfill(2), temps.minutes.zfill(2), temps.seconds.zfill(2))
    