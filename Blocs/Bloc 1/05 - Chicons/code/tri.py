#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""

:author: DIU-Bloc2 - Univ. Lille
:date: 2019, mai

Tris de listes

"""

import random

def compare_entier_croissant(a, b):
    """
    :param a: (int) un entier
    :param b: (int) un entier
    :return: (int)  
             * >0  si a est supérieur à b
             * 0 si a est égal à b
             * <0 si a est inférieur à b
    :CU: aucune
    :Exemples:

    >>> compare_entier_croissant(1, 3) < 0
    True
    """
    return a-b

def compare_entier_décroissant(a, b):
    """
    :param a: (int) un entier
    :param b: (int) un entier
    :return: (int) 
             * >0 si a est inférieur à b
             * 0 si a est égal à b
             * <0 si a est supérieur à b
    :CU: aucune
    :Exemples:

    >>> compare_entier_décroissant(1, 3) > 0
    True
    """
    return b-a
    
def est_trie(l, comp):
    """
    :param l: (type sequentiel) une séquence 
    :param comp: une fonction de comparaison
    :return: (bool) 
      - True si l est triée
      - False sinon
    :CU: les éléments de l doivent être comparables
    :Exemples:

    >>> est_trie([1, 2, 3, 4], compare_entier_croissant)
    True
    >>> est_trie([1, 2, 4, 3], compare_entier_croissant)
    False
    >>> est_trie([], compare_entier_croissant)
    True
    """
    i = 0
    res = True
    while res and i < len(l) - 1:
        res = comp(l[i], l[i+1]) <= 0
        i += 1
    return res

def cree_liste_melangee(n):
    """
    :param n: (int) Longueur de la liste à créer
    :return: une permutation des n premiers entiers
    :CU: n >= 0
    :Exemples:
        
    >>> l = cree_liste_melangee(5)
    >>> len(l)
    5
    >>> sorted(l) == [0, 1, 2, 3, 4]
    True
    """
    l = list(range(n))
    random.shuffle(l)
    return l


################################################
#                  TRI 1                       #
################################################


def tri_1(l, comp):
    """
    :param l: (list) une liste
    :param comp: une fonction de comparaison
    :CU: Les éléments de l doivent être comparables 
   
    :Effet de bord: modifie la liste l en triant ses éléments selon l'ordre 
        défini par comp, du plus petit au plus grand
    :Exemples:
    
    >>> l = [3, 1, 4, 1, 5, 9, 2]
    >>> tri_1(l, compare_entier_croissant)
    >>> l == [1, 1, 2, 3, 4, 5, 9]
    True
    """
    while not est_trie(l, comp):
        i = random.randint(0,len(l)-2)
        if comp(l[i], l[i+1]) > 0:
            tmp = l[i]
            l[i] = l[i+1]
            l[i+1] = tmp

################################################
#                   TRI 2                      #
################################################

def tri_2(l, comp):
    """
    :param l: (list) une liste
    :param comp: une fonction de comparaison
    :CU: Les éléments de l doivent être comparables 
   
    :Effet de bord: modifie la liste l en triant ses éléments selon l'ordre 
        défini par comp, du plus petit au plus grand
    :Exemples:
    
    >>> l = [3, 1, 4, 1, 5, 9, 2]
    >>> tri_2(l, compare_entier_croissant)
    >>> l == [1, 1, 2, 3, 4, 5, 9]
    True
    """
    n = len(l)
    for i in range(1, n):
        tmp = l[i]
        k = i
        while k >= 1 and comp(tmp, l[k - 1]) < 0 :
            l[k] = l[k - 1]
            k = k - 1
        l[k] = tmp


################################################
#                   TRI 3                      #
################################################

def fais_quelque_chose(l, comp, d, f):
    p = l[d]
    ip = d
    for i in range (d+1, f):
        if comp(p, l[i]) > 0:
            l[ip] = l[i]
            l[i] = l[ip+1]
            ip = ip + 1
    l[ip] = p
    return ip

def tri_3(l, comp, d=0, f=None):
    """
    :param l: (list) une liste
    :param comp: une fonction de comparaison
    :param d: un entier >= 0 et < len(l) (0 par défaut)
    :param f: un entier <= len(l) ou None. None équivaut à len(l)
    :param comp: une fonction de comparaison
    :CU: Les éléments de l doivent être comparables 
   
    :Effet de bord: modifie la liste l en triant ses éléments selon l'ordre 
        défini par comp, du plus petit au plus grand
    :Exemples:
    
    >>> l = [3, 1, 4, 1, 5, 9, 2]
    >>> tri_3(l, compare_entier_croissant)
    >>> l == [1, 1, 2, 3, 4, 5, 9]
    True
    """
    if f is None: f = len(l)
    if f - d > 1:
        ip = fais_quelque_chose(l, comp, d, f)
        tri_3(l, comp, d=d, f=ip)
        tri_3(l, comp, d=ip+1, f=f)

def tri_selection(l, comp):
    """
    :param l: (list) une liste
    :param comp: une fonction de comparaison
    """
    n = 0
    while not(est_trie(l, comp)):
        p  = l[0]
        ip = 0
        for i in range(1, len(l)-n):
            if comp(p, l[i]) < 0:
                p = l[i]
                ip = i
        l[len(l)-n-1], l[ip] = l[ip], l[len(l)-n-1]
        n=n+1
            
def tri_peigne(l, comp):
    intervalle = len(l)
    bChange = True
    
    while intervalle > 1 or bChange:
        intervalle = int(intervalle//1.3)
        if intervalle < 1:
            intervalle = 1
            
        i = 0
        bChange = False
        
        while i < len(l)-intervalle:
            if comp(l[i], l[i+intervalle]) > 0:
                l[i], l[i+intervalle] = l[i+intervalle], l[i]
                bChange = True
            i = i+1
    
def compare_chaine_lexicographique(a,b):
    return not(a <= b)
 
def fusion(A, B):
    if A == []:
        return B
    if B == []:
        return A
    if A[0] <= B[0]:
        return A[0] + fusion(A[1:], B)
    else:
        return B[0]
    
def triFusion(L):
    n = len(L)
    if n <= 1:
        return L
    else:
        return fusion(L[0:int(n//2)], L[int(n//2)+1, len(L)+1])
    
from time import time
L = cree_liste_melangee(10000)
##a = time()
##tri_1(L.copy(), compare_entier_croissant)
##b = time()
##print("Tri 1 : ", b-a)
##a = time()
##tri_2(L.copy(), compare_entier_croissant)
##b = time()
##print("Tri 2 : ", b-a)
##a = time()
##tri_3(L.copy(), compare_entier_croissant)
##b = time()
##print("Tri 3 : ", b-a)
##a = time()
##tri_selection(L.copy(), compare_entier_croissant)
##b = time()
##print("Tri 4 : ", b-a)
##a = time()
##tri_peigne(L.copy(), compare_entier_croissant)
##b = time()
##print("Tri 5 : ", b-a)
