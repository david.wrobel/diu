import Time
import Competitor
from tri import tri_peigne

def read_competitors(filename):
    f = open(filename, 'r', encoding='utf8')
    count = 1
    coureurs = dict()
    
    f.readline() #On ne lit pas la première ligne
    buffer = f.readlines()
    
    f.close()
    
    for ligne in buffer:
        inscrit = ligne.rstrip().split(';')
        coureurs.update({count : Competitor.create(*inscrit, count)})
        count = count+1
        
    return coureurs
        
def to_string(competiteurs):
    for dossard in competiteurs:
        try:
            print("{} => {}.".format(Competitor.to_string(competiteurs[dossard]).ljust(40), Time.to_string(Competitor.get_performance(competiteurs[dossard]))))
        except AttributeError:
            print("{}".format(Competitor.to_string(competiteurs[dossard])))
            
def select_competitor_by_bib(competiteurs, bib):
    try:
        return competiteurs[bib]
    except KeyError:
        return None
    
def select_competitor_by_birth_year(competiteurs, year):
    list_to_ret = []
    
    for inscrit in competiteurs:
        if Competitor.get_birthdate(competiteurs[inscrit]).endswith(str(year)):
            list_to_ret.append(competiteurs[inscrit])
            
    return list_to_ret

def select_competitor_by_name(competiteurs, name):
    list_to_ret = []
    
    for inscrit in competiteurs:
        if Competitor.get_lastname(competiteurs[inscrit]).lower() in name.lower():
            list_to_ret.append(competiteurs[inscrit])
            
    return list_to_ret

def read_performances(filename):
    f = open(filename, 'r', encoding='utf8')
    perf = dict()
    
    f.readline()
    buffer = f.readlines()
    
    f.close()
    
    for ligne in buffer:
        p = ligne.rstrip().split(';')
        perf.update( { int(p[0]) : Time.create(*p[1:]) } ) 
        
    return perf

def set_performances(fileCompetitor, filePerf):
    comp = read_competitors(fileCompetitor)
    perf = read_performances(filePerf)
    
    for p in perf:
        Competitor.set_performance(comp[p], perf[p])
        
    return comp

def sort_competitors_by_lastname(competiteurs):
    li = [competiteurs[comp] for comp in competiteurs]
    
    tri_peigne(li, Competitor.compare_lastname)
    
    return {comp['bib_num'] : comp for comp in li}


def sort_competitors_by_perf(competiteurs):
    
    li = [competiteurs[comp] for comp in competiteurs if Competitor.get_performance(competiteurs[comp]) != None]
    li_noperf = [competiteurs[comp] for comp in competiteurs if Competitor.get_performance(competiteurs[comp]) == None]
    
    tri_peigne(li, Competitor.compare_perf)
    tri_peigne(li_noperf, Competitor.compare_lastname)
    
    li_ret = li + li_noperf
    
    return {comp['bib_num'] : comp for comp in li_ret}

def save_result(comp, file):
    f = open('{}.csv'.format(file), 'w', encoding='utf8')
    f.write('Num_dossard;Prénom;Nom;Performance\n')
    
    for doss in comp:
        try:
            f.write('{};{};{};{}\n'.format(Competitor.get_bib_num(comp[doss]), Competitor.get_firstname(comp[doss]), Competitor.get_lastname(comp[doss]),Time.to_string(Competitor.get_performance(comp[doss]))))
        except AttributeError:
            f.write('{};{};{}\n'.format(Competitor.get_bib_num(comp[doss]), Competitor.get_firstname(comp[doss]), Competitor.get_lastname(comp[doss])))
        
    f.close()
    
def select_competitor(competiteurs, filtre):
    return [competiteurs[comp] for comp in competiteurs if filtre(competiteurs[comp])]

def is_comp_by_year(comp, year):
    return Competitor.get_birthdate(comp).endswith(str(year))

def select_by_year(competiteurs, year):
    return select_competitor(li, lambda c:is_comp_by_year(c,year))
    