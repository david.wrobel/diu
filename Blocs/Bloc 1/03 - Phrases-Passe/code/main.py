from liste_7776_mots import LISTE_MOTS

def non_accentuee(chaine):
    """
    Vérifie si chaine contient des caractères accentués.
    
    :param chaine: (str) une chaine de caractère
    :return: - True si chaine ne contient pas d'accent
             - False sinon
    :CU: chaine est un mot qui ne contient que des lettres.
    
    >>> non_accentuee("David")
    True
    >>> non_accentuee("Téléphone")
    False
    >>> non_accentuee("David-Wrobel")
    False
    """
    
    # car = {'a', 'b', ..., 'z'}
    car = set([chr(k) for k in range(ord('a'), ord('z')+1)])
    # on convertit en minuscule (et en ensemble)
    # "DaViD" devient {'d', 'a', 'v', 'i'}
    chaine_min = set(chaine.lower())
    
    return chaine_min.intersection(car) == chaine_min

import doctest
doctest.testmod(verbose = True)