#!/usr/bin/python3
# -*- coding: utf-8 -*-
from random import random
from collections import namedtuple

Joueur = namedtuple('Joueur', ['Nom', 'Prenom', 'IA', 'num'])

def creer_joueur():
    
    n1 = input("Entrer un nom pour le joueur 1 : ")
    p1 = input("Entrer un prénom pour le joueur 1 : ")
    i1 = not(input("Est-ce un humain (o/n) ? ") == 'o')
    
    n2 = input("Entrer un nom pour le joueur 2 : ")
    p2 = input("Entrer un prénom pour le joueur 2 : ")
    i2 = not(input("Est-ce un humain (o/n) ? ") == 'o')
    
    
    return [Joueur(n1, p1, i1, 1), Joueur(n2, p2, i2, 2)]
    
def joueur_suivant(liste_joueurs, joueur_act):
    """
        Change de joueur
    """
    if joueur_act == liste_joueurs[0]:
        return liste_joueurs[1]
    else:
        return liste_joueurs[0]

def coeff(joueur, joueur_act):
    if joueur == joueur_act:
        return 1
    else:
        return -1
    
def min_max(situation, profondeur, joueur, joueur_act, liste_joueurs):
    if game.est_terminee(situation) or profondeur == 0:
        #print(situation, profondeur, game.evaluation(situation,joueur), coeff(joueur,joueur_act))
        return 0, game.evaluation(situation, joueur)*coeff(joueur, joueur_act)
    else:
        liste = game.possibilites_de_jeu(situation, joueur)
        liste_situations = [game.faire_le_coup(situation, joueur, l) for l in liste]
        
        if joueur == joueur_act:
            dic_max = { i : min_max(liste_situations[i], profondeur-1, joueur_suivant(liste_joueurs, joueur), joueur, liste_joueurs)[1] for i in range(len(liste_situations))}
            #print("dic max : ", dic_max)
            ind = max(dic_max, key=dic_max.get)
            valeur = dic_max[ind]
        else:
            dic_min = { i : min_max(liste_situations[i], profondeur-1, joueur_suivant(liste_joueurs, joueur), joueur, liste_joueurs)[1] for i in range(len(liste_situations))}
            #print("dic min : ", dic_min)
            ind = min(dic_min, key=dic_min.get)
            valeur = dic_min[ind]

        return ind, valeur
        
    
def launch_game():
    
    bTerminee = False
    
    liste_joueurs = creer_joueur()
    situation = game.initialiser_jeu()
    
    joueur_act = game.choisir_joueur(liste_joueurs)
    
    while not(bTerminee):
        print("\nC'est au tour de {} {} de jouer".format(joueur_act.Nom, joueur_act.Prenom))
        game.afficher_le_jeu(situation)
        liste = game.possibilites_de_jeu(situation, joueur_act)
        game.afficher_liste_choix(liste)
        
        if joueur_act.IA :
            c = min_max(situation, 10, joueur_act, joueur_act, liste_joueurs)[0]+1
            
        else:
            c = int(input('Votre choix (choisir entre 1 et {}) : '.format(len(liste))))      
        
        try:
            #print("Vous allez retirer {} bâtons.".format(liste[c-1]))
            situation = game.faire_le_coup(situation, joueur_act, liste[c-1])
            bTerminee = game.est_terminee(situation)
            
            if not(bTerminee):
                joueur_act = joueur_suivant(liste_joueurs, joueur_act)
            
        except IndexError:
            print("Mauvais choix")
            
        
    game.afficher_le_jeu(situation)
    gagnant = game.joueur_gagnant(situation, liste_joueurs, joueur_act)
    
    if gagnant != None:
        print("Le gagnant est {} {}.".format(gagnant.Nom, gagnant.Prenom))
    else:
        print("Partie nulle.")
    
if __name__ == '__main__':
    import sys
    if len(sys.argv) < 2:
        module = 'nim'
    else:
        module = sys.argv[1]
    
    game = __import__(module)
    launch_game()

    