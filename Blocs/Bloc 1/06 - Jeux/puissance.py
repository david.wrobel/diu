#!/usr/bin/python3
# -*- coding: utf-8 -*-

from random import random

def initialiser_jeu():
    """
        return : situation
    """
    jeu = []
    for _ in range(6):
        ligne = []
        for _ in range(7):
            ligne.append(0)
        jeu.append(ligne)
        
    return jeu


def choisir_joueur(liste):
    """
        liste : liste de joueurs
        return : joueur
    """
    return liste[0] if random() < 0.5 else liste[1]

def afficher_le_jeu(situation):
    """
        affichage
        return : None
    """
    for i in range(6):
        for j in range(7):
            if situation[i][j] == 0:
                car = '_'
            else:
                car = situation[i][j]
            print(' {} '.format(car), end= '')
        print()
    return

def possibilites_de_jeu(situation, joueur):
    """
        return : Liste des possibilités
    """
    liste = []
    for j in range(7):
        if situation[0][j] == 0:
            liste.append(j+1)
            
    return liste

def afficher_liste_choix(liste):
    """
        return None
    """
    print("Vous pouvez jouer en colonne : ")
    for i in range(len(liste)):
        if i == len(liste)-1:
            print('Choix {} : {}.'.format(i+1, liste[i]))
        else:
            print('Choix {} : {},'.format(i+1, liste[i]), end = ' ')

def faire_le_coup(situation, joueur, choix):
    """
        choix : numéro de colonne à jouer de 1 à 7
        return : Nouvelle situation
    """
    index = 5
    j = choix - 1
    
    while situation[index][j] != 0:
        index = index - 1
        
    if joueur.num == 1:
        situation[index][j] = 'R'
    else:
        situation[index][j] = 'J'
    
    return situation

def est_terminee(situation):
    """
        return Boolean
    """
    return verif(situation) != 0 or not(0 in situation[0])

def verif_horizontal(situation):
    for i in range(6):
        for j in range(4): #Inutile d'aller au bout de la ligne
            if situation[i][j] != 0 and situation[i][j] == situation[i][j+1] == situation[i][j+2] == situation[i][j+3]:
                return situation[i][j]
    return 0

def verif_vertical(situation):
    for j in range(7):
        for i in range(3): #Inutile d'aller au bout de la colonne
            if situation[i][j] != 0 and situation[i][j] == situation[i+1][j] == situation[i+2][j] == situation[i+3][j]:
                return situation[i][j]
    return 0

def verif_diagonale_gauche(situation):
    for j in range(4):
        for i in range(3):
            if situation[i][j] != 0 and situation[i][j] == situation[i+1][j+1] == situation[i+2][j+2] == situation[i+3][j+3]:
                return situation[i][j]
    return 0

def verif_diagonale_droite(situation):
    for j in range(3, 7):
        for i in range(3):
            if situation[i][j] != 0 and situation[i][j] == situation[i+1][j-1] == situation[i+2][j-2] == situation[i+3][j-3]:
                return situation[i][j]
    return 0    
    
def verif(situation):
    ret = verif_horizontal(situation)
    if ret == 0:
        ret = verif_vertical(situation)
        if ret == 0:
            ret = verif_diagonale_gauche(situation)
            if ret == 0:
                ret = verif_diagonale_droite(situation)
                if ret == 0:
                    return 0
    return ret

def joueur_gagnant(situation, liste, joueur):
    """
        situation du jeu
        liste des joueurs
        joueur actuel
        return : joueur gagnant ou null
    """
    ret = verif(situation)
    
    if ret == 0:
        return None
    elif ret == 'R':
        return liste[0]
    else:
        return liste[1]