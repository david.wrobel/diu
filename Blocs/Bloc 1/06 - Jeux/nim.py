#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Jeu de NIM
"""
from random import random

def initialiser_jeu ():
    """
    Initialisation du jeu
    :param: None 
    :return : (int)
    """
    return int(input ('entrer le nombre de tiges pour commencer:'))


def choisir_joueur(liste):
    """
        liste : liste de joueurs
        return : joueur
    """
    return liste[0] if random() < 0.5 else liste[1]


def afficher_le_jeu(situation):
    """
        affichage
        return : None
    """
    chaine_jeu = ""
    for _ in range(situation):
        chaine_jeu += "| "
    print(chaine_jeu)
    return

def possibilites_de_jeu(situation, _):
    """
        return : Liste des possibilités
    """
    choix =[]
    if situation > 2 :
        choix = [2, 3]
    else:
        choix = [2]
    return (choix)

def afficher_liste_choix(liste):
    """
        return None
    """
    if len(liste)==2:
        print("Vous pouvez retirer :\n solution 1 : {} ,\n solution 2 : {}.".format(liste[0],liste[1]))
    elif len(liste)==1:
        print("Vous pouvez retirer :\n solution 1 : {}.".format(liste[0]))
    else:
        print("Aucun choix")
    return
          

def faire_le_coup(situation,_ , choix):
    """
        choix : suivant le jeu
        return : Nouvelle situation
    """
    situation -=  choix
    return situation

def est_terminee(situation):
    """
        return Boolean
    """
    return (situation <= 1)

def joueur_gagnant(situation, _, joueur):
    """
        situation du jeu
        liste des joueurs
        joueur actuel
        return : joueur gagnant ou null
    """
    if situation == 1 :
        return None
    elif situation == 0 :
        return (joueur)
    
def evaluation(situation, joueur):
    if situation == 1:
        return 0
    elif situation == 0:
        return 1
    else:
        return 0