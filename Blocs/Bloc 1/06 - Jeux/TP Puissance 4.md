# **TP Puissance 4**

#### <u>Objectifs:</u>

- programmer un jeu connu
- mettre en œuvre une structure de données et savoir l'utiliser

#### <u>Présentation du jeu:</u>

**Puissance 4** (appelé aussi parfois **4 en ligne**) est un [jeu de stratégie combinatoire abstrait](https://fr.wikipedia.org/wiki/Jeu_de_stratégie_combinatoire_abstrait), commercialisé pour la première fois en 1974 par la [Milton Bradley Company](https://fr.wikipedia.org/wiki/Milton_Bradley_Company), plus connue sous le nom de [MB](https://fr.wikipedia.org/wiki/Milton_Bradley_Company) et détenue depuis 1984 par la société [Hasbro](https://fr.wikipedia.org/wiki/Hasbro).



![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/dc/Puissance4_01.svg/220px-Puissance4_01.svg.png)

#### Règles du jeu

Le but du jeu est d'aligner une suite de 4 pions de même couleur sur une grille comptant 6 rangées et 7 colonnes. Chaque joueur dispose de 21 pions d'une couleur (par convention, en général jaune ou rouge). Tour à tour, les deux joueurs placent un pion dans la colonne de leur choix, le pion coulisse alors jusqu'à la position la plus basse possible dans la dite colonne à la suite de quoi c'est à l'adversaire de jouer. Le vainqueur est le joueur qui réalise le premier un alignement (horizontal, vertical ou diagonal) consécutif d'au moins quatre pions de sa couleur. Si, alors que toutes les cases de la grille de jeu sont remplies, aucun des deux joueurs n'a réalisé un tel alignement, la partie est déclarée nulle.



#### <u>**Etape 0 : préparation de l'environnement de travail**</u> 

Récupérez dans le dossier **TP Puissance 4** :

- **play.py**  que vous aller compléter
- **puissance.py** que vous allez compléter aussi.

#### <u>Etape 1 : examen du fichier play.py</u>

Au début du fichier une instructions d'importation de fonction  

```python
from collections import namedtuple
```

suivit de la definition:

```python
Joueur = namedtuple('Joueur', ['Nom', 'Prenom', 'IA', 'num'])
```

Qu’est-ce qu’un namedtuple et à quoi ça sert ?

Les tuples, vous connaissez : c’est une séquence ordonnées **non modifiable** d’éléments hétérogènes.

Pardon, je recommence. C’est comme une liste, mais plus rapide, et pas modifiable. Ca se créer avec l’opérateur “`,`” :

```python
>>> un_tuple = 1, 2, 3
>>> print(un_tuple)
(1, 2, 3)
>>> type(un_tuple)
<type 'tuple'>
>>> un_tuple[0]
1
>>> un_tuple[:-1]
(1, 2)
>>> autre_tuple = ("pomme", 1, True)
```

Les parenthèses sont optionnelles mais permettent d’éviter les ambiguïtés.

Bref, les tuples, c’est top : on peut les slicer, les indexer, et en plus c’est très très rapide car non modifiable .

Le tuple est donc une bonne alternative aux listes si on sait que le contenu ne bougera pas. Ca prend moins de mémoire, c’est plus rapide, c’est tout aussi flexible, et comme c’est un itérable on peut le plugger un peu partout.

Un gros défaut du tuple, néanmoins, c’est l’absence d’attributs nommés. Il faut connaitre les indices des valeurs, et c’est pas évident à lire si le tuple est gros.

Pour cette raison, la lib standard vient avec le namedtuple, qui est simplement un tuple dont les valeurs sont nommées.

Ca s’utilise en deux étapes. D’abord, définir un nouveau type de namedtuple avec la liste des attributs qu’il va contenir. 

```python
>>> from collections import namedtuple
>>> Joueur = namedtuple('Joueur', ['nom', 'prénom', 'IA','num'])
```

À partir de là nous avons une nouvelle “classe” de namedtuple appelée `Joueur` qui peut créer des namedtuples avec 4 attributs : ‘nom’, ‘prénom’ , ‘IA’ et 'num'.

Et ça s’utilise comme un tuple :

```
>>> joueur_act = Joueur('Grenier', 'Thomas', False, 1)
>>> joueur_act[0] 
'Grenier'
```

Mais aussi comme un objet  :

```
>>> joueur_act.nom
'Grenier'
>>> sam = Joueur(nom="Sam", prénom="quentin", IA=False, num = 2)
>>> sam.num
2
```

Complétez la doctring de la fonction suivante et expliquez ce qu'elle fait ?

```python
def creer_joueur():
    """

    """    
    n1 = input("Entrer un nom pour le joueur 1 : ")
    p1 = input("Entrer un prénom pour le joueur 1 : ")
    i1 = not(input("Est-ce un humain (o/n) ? ") == 'o')
    
    n2 = input("Entrer un nom pour le joueur 2 : ")
    p2 = input("Entrer un prénom pour le joueur 2 : ")
    i2 = not(input("Est-ce un humain (o/n) ? ") == 'o')
    
    return [Joueur(n1, p1, i1, 1), Joueur(n2, p2, i2, 2)]
```

Complétez la doctring de la fonction suivante et expliquez ce qu'elle fait ?

```python
def joueur_suivant(liste_joueurs, joueur_act):
    """
        
    """
    if joueur_act == liste_joueurs[0]:
        return liste_joueurs[1]
    else:
        return liste_joueurs[0]
```



#### <u>Etape 2 : Procédure principale du jeu:</u>

On remarquera au début du fichier play.py l

```
def lancer_jeu():
    """
    Lancement du jeu
    
    """
    bTerminee = False
    liste_joueur = creer_joueur()
    situation = game.initialiser_jeu()
    joueur_act = game.choisir_joueur(liste_joueur)
    
    while not(bTerminee):
        print("\nC'est au tour de {} {} de jouer".format(joueur_act.Nom, joueur_act.Prenom))
        game.afficher_le_jeu(situation)
        liste = game.possibilites_de_jeu(situation, joueur_act)
        game.afficher_liste_choix(liste)
        
        c = int(input('Votre choix (choisir entre 1 et {}) : '.format(len(liste))))
        
        situation = game.faire_le_coup(situation, joueur_act, liste[c-1])
        bTerminee = game.est_terminee(situation)
        
        if not(bTerminee):
            joueur_act = joueur_suivant(liste_joueur, joueur_act)
     
    game.afficher_le_jeu(situation)
    gagnant = game.joueur_gagnant(situation, liste_joueur, joueur_act)
    
    if gagnant != None:
        print("Le gagnant est {} {}.".format(gagnant.Nom, gagnant.Prenom))
    else:
        print("Partie nulle.")
```

Mettre en évidence ( entourer en rouge, toutes les fonctions qui font appellent au module dans puissance.

#### <u>Etape 3 : module spécifique au jeu puissance4 :</u>

Pour les différentes fonctions demandées : 

- vous rédigerez le code python,
- vous écrirez les doctests utiles.

1. **<u>la situation initial</u>**

Le tableau de jeu est une grille de dimension 7 x 6. Réaliser la fonction initialiser_jeu qui rend aucun paramètre et qui renvoie une grille 7 x 6 remplie de '0'.

```python
def initialiser_jeu():
    """
        initialisation du jeu de puissance 4
        Il s'agit de réaliser une grille de dimension 7 en largeur et 6 en hauteur
        :return : (list) situation du jeu 
    """
```

2. **<u>affichage de la situation</u>**

Cette fonction aura pour objectif d'afficher le contenu de chaque case de la grille de la manière suivante:

0    0    0    0    0    0    0

0    0    0    0    0    0    0

0    0    0    0    0    0    0

0    J     R    J     R    0    0

J    R    R    R     J     J     J

J    R     J      R    J    R    R

```python
def afficher_le_jeu(situation):
    """
        affichage du jeu
        : param: (list) situation de jeu
        return : None
    """
```

3. **<u>Choisir un premier joueur:</u>**

Réalisez cette fonction qui permet de choisir aléatoirement le joueur qui commence la partie.

Cette fonction utilisera une liste des joueurs, est renvoie un joueur.

```
def choisir_joueur(liste):
    """
        Décider du joueur qui commence la partie
        :param :(list) liste de joueurs
        :return :(tuple) joueur
    """
```

4. <u>**propose toutes les possibilités de jouer:**</u>

   Cette fonction va proposer au joueur actuel toutes les possibilités qu'il a de jouer. Elle utilise en paramètres .

   ```python
   def possibilites_de_jeu(situation, joueur):
       """
           param: (list) situation de jeu
           param: (tuple) joueur
           return : Liste des possibilités
       """
   ```

   

5. <u>**affichage des possibilités de jouer:**</u>

   cette fonction affiche les possibilités de jouer:

   ```python
   def afficher_liste_choix(liste):
       """
           :param: (liste)
           :return: None
       """
   ```

   

6. <u>**réaliser la nouvelle situation:**</u>

   cette fonction va définir la nouvelle situation de jeu lorsque le joueur actuel aura fait le choix de jeu.

   Elle aura en paramètre l'ancienne situation, le joueur actuel et le choix du joueur. Elle retournera la nouvelle situation.

   ```
   def faire_le_coup(situation, joueur, choix):
       """
           Modifier la situation de jeu en fonction du choix fait par le joueur.
           :param: (list) situation de jeu
           :param: (tuple) joueur actuel
           :param:choix : (int) numéro de colonne à jouer de 1 à 7
           :return :(list)  nouvelle situation de jeu
       """
   ```

   

7. <u>**observer 4 pions alignés horizontalement**</u>

   Détectez lorsque 4 pions sont alignés horizontalement à partir de la situation. la réponse sera une valeur entière.

   ```
   def verif_horizontal(situation):
       """
       Verification de 4 pions identiques à la suite à l'horizontale.
       param: (list) situation de jeu
       return : (int) couleur des poins alignés
       """
   ```

   

8. <u>**observer 4 pions alignés verticalement**</u>

   Détectez lorsque 4 pions sont alignés verticalement à partir de la situation. la réponse sera une valeur entière.

9. <u>**observer 4 pions alignés en diagonale1**</u>

   Travail identique à la fonction précédente pour un alignement en diagonale.

10. **<u>observer 4 pions alignés en diagonale 2</u> **

    Travail identique à la fonction précédente pour un alignement en diagonale opposé au précédent.</u>

11. <u>**détecter la fin de la partie**</u>

    Cette fonction doit détecter la fin de la partir à partir d'une situation. Elle renvoie alors un booléen ( True or False)

    ```python
    def est_terminee(situation):
        """
            Determination de la fin de partie
            : param: (list) situation de jeu
            :return: (Boolean)
        """
    ```

    

12. <u>**définir le joueur gagnant**</u>

    Cette fonction renvoie le joueur gagnant en fin de partie à partir de la situation finale , de la liste des joueurs, et du joueur actuel.

```
def joueur_gagnant(situation, liste, joueur):
    """
        Designer le joueur gagnant.
        param: (list) situation du jeu
        param: (list) liste des joueurs
        param : (tuple) joueur actuel
        return : (tuple)joueur gagnant ou null
    """
```

