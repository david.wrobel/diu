# Les différentes techniques de tri

## Présentation générale

### Le tri par insertion

![Tri par insertion](https://upload.wikimedia.org/wikipedia/commons/thumb/0/0f/Insertion-sort-example-300px.gif/220px-Insertion-sort-example-300px.gif)

```python
def tri_insertion(l):
    """
        :param l: une liste à trier
        :return:  None
        :effets de bord: la liste l est triée
    """
    n = len(l)
    for i in range(1, n):
        x = l[i]
        j = i
        while (j>0) and (l[j-1]>x):
            l[j] = l[j-1]
            j -= 1
        l[j] = x
    return None
```

### Le tri par sélection

![Tri par sélection](https://upload.wikimedia.org/wikipedia/commons/9/94/Selection-Sort-Animation.gif)

```python
def tri_selection(l):
    """
        :param l: une liste à trier
        :return:  None
        :effets de bord: la liste l est triée
    """
    n = len(l)
    for i in range(0, n-1):
        mini = i
        for j in range(i+1, n):
            if (l[j] < l[mini]) :
                mini = j
        if mini != i:
            l[i], l[mini] = l[mini], l[i]
    return None
```

### Le tri fusion

![Le tri fusion](https://upload.wikimedia.org/wikipedia/commons/6/60/Mergesort_algorithm_diagram.png)

```python
def fusion(l1, l2):
    """
        :param l1, l2: deux listes à fusionner
        :return: une liste triée
    """
    if l1 == []:
        return l2
    if l2 == []:
        return l1
    if (l1[0] <= l2[0]) :
        return [l1[0]] + fusion(l1[1:len(l1)], l2)
    else:
        return [l2[0]] + fusion(l1, l2[1:len(l2)])
        
    
def tri_fusion(l):
    """
        :param l: une liste à trier
        :return:  une liste triée à partir de l
        :effets de bord: aucun
    """
    n = len(l)
    if (n <= 1):
        return l
    else:
        # L[a:b] retourne la liste formée des éléments de L indéxés de a à b-1
        return fusion(tri_fusion(l[0:n//2]), tri_fusion(l[n//2:n]))
```

### Le tri à bulles

![Tri à bulles](https://upload.wikimedia.org/wikipedia/commons/5/54/Sorting_bubblesort_anim.gif)

```python
def tri_bulles(l):
    """
        :param l: une liste à trier
        :return:  None
        :effets de bord: la liste l est triée
    """
    n = len(l)
    for i in range(n-1,-1+1,-1):
        for j in range(0, i-1+1):
            if l[j+1] < l[j]:
                l[j], l[j+1] = l[j+1], l[j] # On échange
                
    return None
```

### Le tri à peigne

![Le tri à peigne](https://upload.wikimedia.org/wikipedia/commons/4/46/Comb_sort_demo.gif)

```python
def tri_peigne(l):
    """
        :param l: une liste à trier
        :return:  None
        :effets de bord: la liste l est triée
    """
    n = len(l)
    intervalle = len(l)
    echange = True
    
    while (intervalle > 1) and echange:
        intervalle = max(int(intervalle//1.3), 1)
        
        i = 0
        echange = False
        
        while (i < n - intervalle):
            if l[i] > l[i+intervalle]:
                l[i], l[i+intervalle] = l[i+intervalle], l[i]
                echange = True
            i = i+1
    return None
```

### Le tri rapide

![Le tri rapide](https://upload.wikimedia.org/wikipedia/commons/6/6a/Sorting_quicksort_anim.gif)

```python
from random import randint

def partitionner(l, prem, dern, piv):
    """
        :param l: une liste à trier
        :param prem: index du premier élément
        :param dern: index du dernier élément
        :param pivot: index du pivot
        :return: index du premier élément supérieur au pivot
    """
    l[dern], l[piv] = l[piv], l[dern]
    j = prem
    for i in range(j, dern-1+1):
        if l[i] <= l[dern]:
            l[i], l[j] = l[j], l[i]
            j = j+1
    l[j], l[dern] = l[dern], l[j]
    return j

def tri_rapideBis(l, prem, dern):
    """
        :param l: une liste à trier
        :param prem: index du premier élément
        :param dern: index du dernier élément
        :return:  None
        :effets de bord: la liste l est triée
    """
    if prem < dern:
        pivot = randint(prem, dern)
        pivot = partitionner(l, prem, dern, pivot)
        tri_rapideBis(l, prem, pivot-1)
        tri_rapideBis(l, pivot+1, dern)
        
def tri_rapide(l):
    tri_rapideBis(l, 0, len(l)-1)
```

# Tests des différents tris

```python
from time import time
from random import shuffle
from os import sys

sys.setrecursionlimit(1000000)

tri = [tri_insertion, tri_selection, tri_fusion, tri_bulles, tri_peigne, tri_rapide]
tailles = [10**k for k in range(1, 4+1)]

for i in range(0, 4):
    L = list(range(1, tailles[i]))
    shuffle(L)
    print("Début du tri pour une liste à {} éléments".format(tailles[i]))
    
    for j in range(0, 6):
        a = time()
        tri[j](L.copy())
        b = time()
        print("Tri {} : {} secondes.".format(j+1, b-a))
```