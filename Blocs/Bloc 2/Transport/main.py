from TSP_biblio import *

def matrice_distance(tour):
    nb_ville = len(tour)
    mat = []
    
    for i in range(nb_ville):
        dist = []
        for j in range(nb_ville):
            dist.append(distance(tour, i, j))
        mat.append(dist)
        
    return mat

def get_index(ville, nom_ville):
    i = 0
    while ville[i][0] != nom_ville:
        i=i+1
    return i

def ville_plus_proche(ville, nom_ville):
    i = get_index(ville, nom_ville)
    
    A = matrice_distance(ville)[i]
    j = A.index(min([a for a in A if a > 0]))
       
    return ville[j][0]

def parcours(ville, depart):
    tour = [ville[get_index(ville, depart)]]
    vact = depart
    
    while( len(ville) != 1 ):
        vpp = ville_plus_proche(ville, vact)
        tour.append(ville[get_index(ville, vpp)])
        ville.remove(ville[get_index(ville, vact)])
        vact = vpp
    
    tour.append(ville[0])
    return tour

if __name__ == '__main__':
    liste = get_tour_fichier('exemple.txt')
    tour = parcours(liste, 'Annecy')
    trace(tour)

    
    