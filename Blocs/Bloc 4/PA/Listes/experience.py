from extendedlist import *
from random import randint
import timeit

def somme_des_elements_a(l):
    s = 0
    for i in range(len(l)):
        s += l[i]
    return s

def somme_des_elements_b(l):
    s = 0
    for e in l:
        s += e
    return s

def get_extendedlist_by_length(n):
    L = Extendedlist(0, Extendedlist())
    for i in range(n-1):
        L = Extendedlist(randint(0,n), L)
    return L
    
def get_list_by_length(n):
    L = []
    for i in range(n):
        L.append(randint(0,n))
    return L

time_ext_a = []
time_ext_b = []
time_lst_a = []
time_lst_b = []

for _ in [500, 1000, 1500, 2500, 3000]:
    extL = get_extendedlist_by_length(_)
    time_ext_a.append(timeit.timeit(lambda : somme_des_elements_a(extL), number = 1))
    time_ext_b.append(timeit.timeit(lambda : somme_des_elements_b(extL), number = 1))
    
    lstL = get_list_by_length(_)
    time_lst_a.append(timeit.timeit(lambda : somme_des_elements_a(lstL), number = 1))
    time_lst_b.append(timeit.timeit(lambda : somme_des_elements_b(lstL), number = 1))
    