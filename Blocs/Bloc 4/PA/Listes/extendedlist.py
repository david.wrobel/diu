from otherlist import *

class Extendedlist(List):

    def length (self):
        """
        Return the length of a list
        
        :return: Number of elements in the list

        >>> l = Extendedlist()
        >>> l.length()
        0
        >>> l = Extendedlist(3,l)
        >>> l = Extendedlist(2,l)
        >>> l = Extendedlist(1,l)
        >>> l.length()
        3
        """
        length = 0
        list_buffer = self
        
        while not(list_buffer.is_empty()):
            length += 1
            list_buffer = list_buffer.tail()
            
        return length
    
    def get(self,i):
        '''
        Get the element at position i (positions start at 0).
        
        :CU: not self.is_empty()

        >>> l = Extendedlist(1, Extendedlist())
        >>> l.get(0)
        1
        >>> l = Extendedlist(4, Extendedlist())
        >>> l = Extendedlist(3,l)
        >>> l = Extendedlist(2,l)
        >>> l = Extendedlist(1,l)
        >>> l.get(3)
        4
        >>> l.get(0)
        1
        '''
        if i < self.length() :
            list_buffer = self
            while i > 0:
                list_buffer = list_buffer.tail()
                i -= 1
            return list_buffer.head()
        else:
            return ListError('No entry at this index.')
    
            
    def search (self, e):
        """
        Return whether e exists in the list

        :return: True iff e is an element of the list
        
        >>> l = Extendedlist()
        >>> l.search(0)
        False
        >>> l = Extendedlist(3,l)
        >>> l = Extendedlist(2,l)
        >>> l = Extendedlist(1,l)
        >>> l.search(1)
        True
        >>> l.search(3)
        True
        >>> l.search(4)
        False
        """
        list_buffer = self
        
        while not(list_buffer.is_empty()) :
            if list_buffer.head() == e :
                return True
            list_buffer = list_buffer.tail()
        
        return False
    
    def toString (self):
        """
        Return a string representation of the list

        >>> l = Extendedlist()
        >>> l = Extendedlist(3,l)
        >>> l = Extendedlist(2,l)
        >>> l = Extendedlist(1,l)
        >>> l.toString()
        '1 2 3'
        """
        L = []
        list_buffer = self
        
        while not(list_buffer.is_empty()) :
            L.append(str(list_buffer.head()))
            list_buffer = list_buffer.tail()
            
        return ' '.join(L)

    
    def toPythonList (self):
        """
        Return the Python list corresponding to the list

        :return: A Python list whose length and elements are identical to `self`.

        >>> l = Extendedlist()
        >>> l.toPythonList()
        []
        >>> l = Extendedlist(3,l)
        >>> l = Extendedlist(2,l)
        >>> l = Extendedlist(1,l)
        >>> l.toPythonList()
        [1, 2, 3]
        """
        L = []
        list_buffer = self
        
        while not(list_buffer.is_empty()) :
            L.append(list_buffer.head())
            list_buffer = list_buffer.tail()
            
        return L

    
    def sortedInsert(self, x):
        '''
        Insert element x in the list.
        Return a fresh list.

        CU: the list must be sorted according to '<'

        :return: A fresh sorted Extendedlist containing elements of `self`and `x`

        >>> l = Extendedlist()
        >>> l = Extendedlist(4,l)
        >>> l = Extendedlist(2,l)
        >>> l = Extendedlist(1,l)
        >>> l.sortedInsert(3)
        (1.(2.(3.(4.()))))
        >>> l = Extendedlist(4, Extendedlist())
        >>> l.sortedInsert(10)
        (4.(10.()))
        >>> l = Extendedlist(4, Extendedlist())
        >>> l.sortedInsert(1)
        (1.(4.()))
        '''
        list_to_ret = Extendedlist()
        list_buffer = self
        
        while not(list_buffer.is_empty()) and list_buffer.head() < x :
            list_to_ret = Extendedlist(list_buffer.head(), list_to_ret)
            list_buffer = list_buffer.tail()
            
        list_to_ret = Extendedlist(x, list_to_ret)
        
        while not(list_buffer.is_empty()) :
            list_to_ret = Extendedlist(list_buffer.head(), list_to_ret)
            list_buffer = list_buffer.tail()
            
        return list_to_ret.reverse()

    def reverse(self):
        '''
        :return: A fresh list containing the same elements as `self`but in reversed order.

        >>> l = Extendedlist()
        >>> l = Extendedlist(3,l)
        >>> l = Extendedlist(2,l)
        >>> l = Extendedlist(1,l)
        >>> l
        (1.(2.(3.())))
        >>> r = l.reverse()
        >>> r
        (3.(2.(1.())))
        '''        
        list_to_ret = Extendedlist()
        
        for _ in range(0, self.length()):
            list_to_ret = Extendedlist(self.get(_), list_to_ret)
            
        return list_to_ret
    
    def append (self, e):
        '''
        Append element e at the end of the list.
        Raises Listerror if the list is empty.
       
        >>> l = Extendedlist(1,Extendedlist())        
        >>> l.append(2)
        >>> l
        (1.(2.()))
        >>> l.append(3)
        >>> l
        (1.(2.(3.())))
        '''
        if self.is_empty():
            raise ListError("")
        elif self.tail().is_empty():
            self.set_tail(Extendedlist(e,List()))
        else:
            self.tail().append(e)
            
    def __str__ (self):
        return self.toString()
    
    def __getitem__ (self,i):
        return self.get(i)

    def __len__ (self):
        return self.length()

    def __next__ (self):
        try:            
            v = self.__iter.head()
            self.__iter = self.__iter.tail()
            return v
        except:
            raise StopIteration
        
    def __iter__ (self):
        """
        Implantation très sommaire d'un itérateur. Ne permet pas d'itérer
        sur la même liste dans une boucle imbriquée.
        """
        self.__iter = self
        return self


    
if __name__ == "__main__":
    import doctest
    doctest.testmod()

    
