from stack import Stack
from myqueue import Queue
from html_parser1 import parse

def html_checker(s):
    """
    :param s: (str) un document html
    :return: (bool) True si `s` est bien formé, False sinon
    :CU: Aucune
    :Exemple:

    >>> html_checker("<!DOCTYPE html><html lang='fr'><div><p></p></div></html>")
    True
    >>> html_checker("<!DOCTYPE html><html lang='fr'><div></p></div><p></html>")
    False
    >>> html_checker("<!DOCTYPE html><html lang='fr'><div><p></p></div>")
    False
    """
    balises = parse(s)
    pile = Stack()
    
    while not balises.is_empty():
        buff = balises.dequeue()[1:-1]
        
        if not pile.is_empty():
            if '/' + pile.top() == buff:
                pile.pop()
            else:
                pile.push(buff)
        else:
            pile.push(buff)
        
    return pile.is_empty()
    

if __name__ == "__main__":
    import sys
    if len(sys.argv) <= 1:
        # pas d'arguments sur la ligne de commande
        # on exécute les tests.
        import doctest
        doctest.testmod()
    else:
        # on considère que l'argument est le nom d'un fichier
        fname = sys.argv[1]
        try:
            with open(fname, 'r') as f:
                if html_checker(f.read()):
                    print("le fichier {} est un fichier html correct.".format(fname))
                else:
                    print("le fichier {} n'est pas un fichier html correct.".format(fname))
        except:
            print("Impossible de parser le fichier {}".format(fname))
