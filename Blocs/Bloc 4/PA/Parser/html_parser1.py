from myqueue import Queue

def parse(doc):
    """
    :param doc: (str) a document to be parsed
    :return: (Queue) a queue containing tag (without attributes)
    :CU: None
    :Example:

    >>> q = parse("<!DOCTYPE html><html><div class='titre'>du contenu ignoré</div></html>")
    >>> q.is_empty()
    False
    >>> q.dequeue() == "<html>"
    True
    >>> q.dequeue() == "<div>"
    True
    >>> q.dequeue() == "</div>"
    True
    >>> q.dequeue() == "</html>"
    True
    >>> q.is_empty()
    True
    >>> # Other example
    >>> q = parse("<html><div><p <a")
    >>> q.dequeue() == "<html>"
    True
    >>> q.dequeue() == "<div>"
    True
    >>> q.is_empty()
    True
    """
    balises = Queue()
    index = 0
    i = 0
    
    for i in range(len(doc)-1):
        length = 1
        
        if doc[i] == '<' and doc[i+1] != '!':
            index = i
            try:
                while doc[index+length] != '>':
                    length += 1
                    
                buffer = doc[index:index+length+1].split(' ')
                if len(buffer) == 1:
                    balises.enqueue(doc[index:index+length+1])
                else:
                    balises.enqueue(buffer[0] + '>')
            except IndexError:
                pass
    return balises
        
if __name__ == "__main__":
    import doctest
    doctest.testmod()
