import re
from myqueue import Queue

HTML_REGEX = re.compile(r"""<(/?\w+)((\s+\w+(\s*=\s*(?:".*?"|'.*?'|[\^'">\s]+))?)+\s*|\s*)>""")

def parse(doc):
    """
    :param doc: (str) a document to be parsed
    :return: (Queue) a queue containing tag (without attributes)
    :CU: None
    :Example:

    >>> q = parse("<!DOCTYPE html><html><div class='titre'>du contenu ignoré</div></html>")
    >>> q.is_empty()
    False
    >>> q.dequeue() == "<html>"
    True
    >>> q.dequeue() == "<div>"
    True
    >>> q.dequeue() == "</div>"
    True
    >>> q.dequeue() == "</html>"
    True
    >>> q.is_empty()
    True
    >>> # Other example
    >>> q = parse("<html><div><p <a")
    >>> q.dequeue() == "<html>"
    True
    >>> q.dequeue() == "<div>"
    True
    >>> q.is_empty()
    True
    """
    balises = Queue()
    
    for tag_element in HTML_REGEX.findall(doc):
        balises.enqueue('<'+tag_element[0]+'>')
        
    return balises
        
if __name__ == "__main__":
    import doctest
    doctest.testmod()