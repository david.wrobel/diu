class QueueEmptyError(Exception):
    def __init__(self, msg):
        super().__init__(msg)

class Queue:
    """
    a class representing a queue

    >>> q = Queue()
    >>> q.is_empty()
    True
    >>> q.enqueue(1)
    >>> q.enqueue(2)
    >>> q.is_empty()
    False
    >>> q.dequeue()
    1
    >>> q.dequeue()
    2
    """

    def __init__(self):
        """
        create a new queue
        """
        self.__file  = []

    def is_empty(self):
        """
        :return: (bool) True si la queue est vide, False sinon
        """
        return self.__file == []

    def enqueue(self, el):
        """
        enfile un élément dans la file
        :param el: (any) un élément
        """
        self.__file = [el] + self.__file

    def dequeue(self):
        """
        défile un élément
        :return: (any) un élément
        """
        if self.__file == []:
            raise QueueEmptyError("Erreur de file")
        else:
            ret = self.__file[-1]
            del self.__file[-1]               
            return ret

if __name__ == "__main__":
    import doctest
    doctest.testmod()
