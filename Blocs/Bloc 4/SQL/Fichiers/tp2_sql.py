import sqlite3 as sql
import timeit

def get_informations_from_cp_a(cp):
    conn = sql.connect('categories-socio-nord.db')
    cur = conn.cursor()
    
    cur.execute('''SELECT codeINSEE, codePostal, commune, categorie, genre, effectif FROM correspondance
            JOIN evolution ON correspondance.codeINSEE = evolution.code WHERE INSTR(codePostal, {}) != 0;'''.format(cp))
    res = cur.fetchall()
    conn.close()
    return res

def get_informations_from_cp_b(cp):
    conn = sql.connect('categories-socio-nord.db')
    cur = conn.cursor()
    
    cur.execute('''SELECT codeINSEE, codePostal, commune, categorie, genre, effectif FROM correspondance
            JOIN evolution ON correspondance.codeINSEE = evolution.code;''')
    res = cur.fetchall()
    ville = []
    
    for line in res:
        if str(cp) in line[1]:
            ville.append(line)
            
    conn.close()
    return ville



ta = timeit.timeit(lambda : get_informations_from_cp_a(59650), number = 1)
tb = timeit.timeit(lambda : get_informations_from_cp_b(59650), number = 1)

print('Temps avec WHERE : {}. Temps avec tri : {}.\n'.format(ta, tb))

# On se connecte
conn = sql.connect('categories-socio-nord.db')

# On crée un curseur
cur = conn.cursor()

cur.execute("SELECT ville.code, categorie, genre, effectif FROM ville JOIN evolution ON ville.code = evolution.code WHERE nom = 'Caullery';")
res = cur.fetchall()

for line in res:
    print(line)
    
cur.execute("SELECT SUM(effectif) FROM ville JOIN evolution ON ville.code = evolution.code WHERE departement = 59 AND genre = 'Femmes';")
res = cur.fetchone()

nb_femmes = res[0]

print('Nombre de femmes dans le nord : {}.'.format(nb_femmes))
    
cur.execute("SELECT SUM(effectif) FROM ville JOIN evolution ON ville.code = evolution.code WHERE departement = 59;")
res = cur.fetchone()

print('Le pourcentage de femmes dans le nord est de {:.2f}%.'.format(nb_femmes*100/res[0]))

cur.execute('''SELECT codeINSEE, codePostal, commune, categorie, genre, effectif FROM correspondance
            JOIN evolution ON correspondance.codeINSEE = evolution.code
            WHERE commune = "VILLENEUVE-D'ASCQ";''')

res = cur.fetchall()

for line in res:
    print(line)

# On ferme la connexion
conn.close()