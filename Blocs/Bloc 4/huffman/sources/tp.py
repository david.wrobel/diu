from binary_tree import BinaryTree as BT
import graphviz
from IPython.display import display


def show_tree(arbre):
    display(graphviz.Source(arbre.to_dot()))
    

texte_source = '''La Cigale et la Fourmi

La Cigale, ayant chanté
Tout l'été,
Se trouva fort dépourvue
Quand la bise fut venue :
Pas un seul petit morceau
De mouche ou de vermisseau.
Elle alla crier famine
Chez la Fourmi sa voisine,
La priant de lui prêter
Quelque grain pour subsister
Jusqu'à la saison nouvelle.
Je vous paierai, lui dit-elle,
Avant l'Oût, foi d'animal,
Intérêt et principal. 
La Fourmi n'est pas prêteuse :
C'est là son moindre défaut.
Que faisiez-vous au temps chaud ?
Dit-elle à cette emprunteuse.
Nuit et jour à tout venant
Je chantais, ne vous déplaise.
Vous chantiez ? j'en suis fort aise.
Eh bien! dansez maintenant.
'''

def sequence_to_occurrences(seq):
    '''
    :param seq: une séquence de symboles 
    :return: (dict) un dictionnaire donnant le nombre 
          d'occurrences de chacun des symboles contenu dans seq
    :CU: les symboles de seq doivent pouvoir être une clé d'un dictionnaire.
    :Exemple:
    
    >>> sequence_to_occurrences('Codage de Huffman') == {'C': 1, 'o': 1, 'd': 2, 'a': 2, 'g': 1, 'e': 2, ' ': 2, 'H': 1, 'u': 1, 'f': 2, 'm': 1, 'n': 1}
    True
    '''
    dico = {}
    for c in seq:
        try:
            dico[c] += 1
        except KeyError:
            dico[c] = 1
            
    return dico

occ_source = sequence_to_occurrences(texte_source)

def weight(tree):
    '''
    :param tree: (BinaryTree) un arbre binaire étiqueté par des couples (truc, nombre)
    :return: (int ou float) le poids de l'arbre, (ie le nombre composante droite du couple étiquette de l'arbre)
     '''
    return tree.get_data()[1]

def insert(tree, forest):
    '''
    :param tree: (BinaryTree) un arbre étiqueté par des couples (truc, nombre)
    :param forest: (list) une liste d'arbres binaires étiquetés par des couples (truc, nombre) rangés dans l'ordre décroissant des poids
    :return: (NoneType)
    :side effect: modifie la foret en insérant l'arbre à la première position qui maintient la forêt rangée en ordre décroissant
    '''
    if forest == []:
        forest.append(tree)
    else:
        i = 0
        while i < len(forest) and weight(forest[i]) >= weight(tree):
            i += 1
        forest.insert(i, tree)
        
def huffman_tree(occ):
    '''
    :param occ: (dict) dictionnaires d'occurrences
    :return: (BinaryTree) arbre de codage de Huffman
    '''
    sorted_keys = sorted(occ.keys())
    forest = []
    
    for key in sorted_keys:
        insert(BT((key, occ[key]), BT(), BT()), forest)
        
    while len(forest) > 1:
        arb1 = forest.pop()
        arb2 = forest.pop()
        insert(BT(('', weight(arb1) + weight(arb2)), arb1, arb2), forest)
        
    return forest[0]

def tree_to_coding(tree, code = ''):
    '''
    :param tree: (BinaryTree) un arbre de Huffman
    :return: (dict) une table de codage
    '''
    dico = {}
    
    if tree.is_empty():
        return dico
    else:
        if tree.is_leaf():
            dico[tree.get_data()[0]] = code
        else:
            dico.update(tree_to_coding(tree.get_left_subtree(), code+'0'))
            dico.update(tree_to_coding(tree.get_right_subtree(), code+'1'))
            
    return dico

def encode(source, coding_table):
    '''
    :param source: (str) texte source à encoder
    :param coding_table: (dict) une table de codage
    :return: (str)
    '''
    txt_to_ret = ''
    for c in source:
        txt_to_ret = txt_to_ret + coding_table[c]
        
    return txt_to_ret
    
def decode(encoded_source, tree):
    '''
    :param encoded_source: (str) la chaîne binaire à décoder
    :param tree: (BinaryTree) l'arbre de Huffman du codage utilisé
    :return: (str) le message source décodé
    '''
    txt = ''
    i = 0
    
    while i < len(encoded_source):
        way = tree
        j = 0
        while not way.is_leaf():
            c = encoded_source[i+j]
            if c == '0':
                way = way.get_left_subtree()
            else:
                way = way.get_right_subtree()
            j += 1
        txt = txt + way.get_data()[0]
        i += j
    
    return txt
        
def complete(binary_str):
    '''
    :param binary_str: (str) une chaîne de bits
    :return: (str) une chaîne de bits complétée avec un mot de la forme '10...0' de sorte que la chaîne soit de longueur 8
    :CU: len(binary_str) < 8
    :Exemples:
    
    >>> complete('')
    '10000000'
    >>> complete('010')
    '01010000'
    >>> complete('1010111')
    '10101111'
    '''
    return binary_str + '1' + '0' * (7 - len(binary_str))

def uncomplete(binary_str):
    '''
    :param binary_str: (str) une chaîne de bits
    :return: (str) une chaîne de bits débarrassée du bit 1 le plus à droite ainsi que des bits qui suivent.
    :CU: binary_str contient au moins un 1.
    :Exemples:
    
    >>> uncomplete('1000000000000000')
    ''
    >>> uncomplete('01010000')
    '010'
    >>> uncomplete('10101111')
    '1010111'
    '''
    return binary_str[:binary_str.rindex('1')]


if __name__ == '__main__':
    occ_source = sequence_to_occurrences(texte_source)
    huffman_source = huffman_tree(occ_source)
    #show_tree(huffman_source)
    dic = tree_to_coding(huffman_source)
    
    txt_code = encode(texte_source, dic)