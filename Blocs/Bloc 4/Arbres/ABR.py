import binary_tree as bt

VIDE = bt.BinaryTree()

def feuille(data) :
    return bt.BinaryTree(data, VIDE, VIDE)

def parcours(a):
    if a.is_leaf():
        return [a.get_data()]
    else:
        if not a.get_left_subtree().is_empty():
            if not a.get_right_subtree().is_empty():
                return parcours(a.get_left_subtree()) + [a.get_data()] + parcours(a.get_right_subtree())
            else:
                return parcours(a.get_left_subtree()) + [a.get_data()]
        else:
            if not a.get_right_subtree().is_empty():
                return [a.get_data()] + parcours(a.get_right_subtree())
    
def is_binary_search_tree(a):
    lst = parcours(a)
    
    return lst == lst.sort()

            
def get_node(a, val):
    if a.is_empty():
        return None
    else:
        if a.get_data() == val:
            return a
        else:
            if a.get_data() < val:
                return get_node(a.get_right_subtree(), val)
            else:
                return get_node(a.get_left_subtree(), val)
            

def insert_val(a, val):
    if a.is_empty():
        return bt.BinaryTree(val, VIDE, VIDE)
    else:
        if val <= a.get_data():
            return bt.BinaryTree(a.get_data(), insert_val(a.get_left_subtree(), val), a.get_right_subtree())
        else:
            return bt.BinaryTree(a.get_data(), a.get_left_subtree(), insert_val(a.get_right_subtree(), val))
        
            
ABR1 = bt.BinaryTree(12,
                     bt.BinaryTree(8,
                                   bt.BinaryTree(4, VIDE, VIDE),
                                   bt.BinaryTree(10, VIDE, VIDE)),
                     bt.BinaryTree(19, VIDE, VIDE))

ABR2 = bt.BinaryTree(12, VIDE, VIDE)