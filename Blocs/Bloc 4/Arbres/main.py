import binary_tree as bt

VIDE = bt.BinaryTree()

def feuille(data) :
    return bt.BinaryTree(data, VIDE, VIDE)

def is_operand(a):
    return not(a.get_left_subtree().is_empty())

def type_operand(a):
    if is_operand(a):
        return a.get_data()

def afficher_A(a):
    if not a.is_empty():
        afficher_A(a.get_left_subtree())
        print(a.get_data())
        afficher_A(a.get_right_subtree())

def afficher_B(a):
    if not a.is_empty():
        afficher_B(a.get_left_subtree())
        afficher_B(a.get_right_subtree())
        print(a.get_data())

def afficher_C(a):
    if not a.is_empty():
        print(a.get_data())
        afficher_C(a.get_left_subtree())
        afficher_C(a.get_right_subtree())
        
def print_expr(a):
    if not a.is_empty():
        if is_operand(a):
            
            #cumul = '({}){}({})'.format(print_expr(a.get_left_subtree()), a.get_data(), print_expr(a.get_right_subtree()))
            
            
            op = a.get_data()
            if op == '+':
                cumul = '{}+{}'.format(print_expr(a.get_left_subtree()), print_expr(a.get_right_subtree()))
            elif op == '-':
                if is_operand(a.get_right_subtree()):
                    cumul = '{}-({})'.format(print_expr(a.get_left_subtree()), print_expr(a.get_right_subtree()))
                else:
                    cumul = '{}-{}'.format(print_expr(a.get_left_subtree()), print_expr(a.get_right_subtree()))
            elif op == '*':
                if is_operand(a.get_left_subtree()) and a.get_left_subtree().get_data() != '*':
                    if is_operand(a.get_right_subtree()) and a.get_right_subtree().get_data() != '*':
                        cumul = '({})*({})'.format(print_expr(a.get_left_subtree()), print_expr(a.get_right_subtree()))
                    else:
                        cumul = '({})*{}'.format(print_expr(a.get_left_subtree()), print_expr(a.get_right_subtree()))
                else:
                    if is_operand(a.get_right_subtree()) and not a.get_right_subtree().get_data() in ['*', '/']:
                        cumul = '{}*({})'.format(print_expr(a.get_left_subtree()), print_expr(a.get_right_subtree()))
                    else:
                        cumul = '{}*{}'.format(print_expr(a.get_left_subtree()), print_expr(a.get_right_subtree()))
            elif op == '/':
                if is_operand(a.get_left_subtree()) and a.get_left_subtree().get_data() != '*' :                       
                    if is_operand(a.get_right_subtree()):
                        cumul = '({})/({})'.format(print_expr(a.get_left_subtree()), print_expr(a.get_right_subtree()))
                    else:
                        cumul = '({})/{}'.format(print_expr(a.get_left_subtree()), print_expr(a.get_right_subtree()))
                else:
                    if is_operand(a.get_right_subtree()):
                        cumul = '{}/({})'.format(print_expr(a.get_left_subtree()), print_expr(a.get_right_subtree()))
                    else:
                        cumul = '{}/{}'.format(print_expr(a.get_left_subtree()), print_expr(a.get_right_subtree()))
            return cumul
        else:
            return a.get_data()
    else:
        return cumul

def eval_expr(a, dico_var):
#    chaine = print_expr(a)
#    
#    for key in dico_var:
#        if key in chaine:
#            chaine = chaine.replace(key, str(dico_var[key]))
#    
#    try:
#        return eval(chaine)
#    except ZeroDivisionError:
#        print('Pas de division par zéro !!!')

    if not a.is_empty():
        if is_operand(a):
            if a.get_data() == '+':
                return eval_expr(a.get_left_subtree(), dico_var) + eval_expr(a.get_right_subtree(), dico_var)
            elif a.get_data() == '-':
                return eval_expr(a.get_left_subtree(), dico_var) - eval_expr(a.get_right_subtree(), dico_var)
            elif a.get_data() == '*':
                return eval_expr(a.get_left_subtree(), dico_var) * eval_expr(a.get_right_subtree(), dico_var)
            elif a.get_data() == '/':
                return eval_expr(a.get_left_subtree(), dico_var) / eval_expr(a.get_right_subtree(), dico_var)
        else:
            try:
                return dico_var[a.get_data()]
            except KeyError:
                return a.get_data()
        
# 6 * (x+y) + (y−14)
expression1 = bt.BinaryTree('+',
                               bt.BinaryTree('*', feuille(6), bt.BinaryTree('+', feuille('x'), feuille('y'))),
                               bt.BinaryTree('-', feuille('y'), feuille(14)))

# (x+5) * (y/2)
expression2 = bt.BinaryTree('*',
                               bt.BinaryTree('+', feuille('x'), feuille(5)),
                               bt.BinaryTree('/', feuille('y'), feuille(2)))
# 4 * (x-1)
expression3 = bt.BinaryTree('*',
                               feuille(4),
                               bt.BinaryTree('-', feuille('x'), feuille(1)))

