# Exercices base de données

## Lundi 04 Novembre 2019

### Extraction de données

1. Quels sont les différents libellés des catégories socioprofessionnelles par ordre alphabétique croissant ?
```sql
SELECT DISTINCT categorie FROM evolution ORDER BY categorie;
```

2. Combien de catégories différentes sont utilisées dans ce jeu de données ?
```sql
SELECT DISTINCT COUNT(categorie) AS Nb FROM evolution;
```

3. Quel est le code postal de Caullery ?
```sql
SELECT code FROM ville WHERE nom = "Caullery";
```

4. Affichez toutes les informations de la table évolution pour la ville de Caullery (en utilisant le code postal), triées par effectifs croissants.
```sql
SELECT * FROM evolution WHERE code = 59140 ORDER BY effectif;
```

5. Quels sont les codes postaux des villes ayant des catégories socioprofessionnelles dont les effectifs dépassent 2000 individus ?
```sql
SELECT code FROM evolution WHERE effectif >= 2000;
```

6. Combien y a-t-il de femmes agricultrices dans le Nord ?
```sql
SELECT COUNT(genre) AS Nb FROM evolution WHERE categorie = 'Agriculteurs Exploitants';
```

7. Quel est le nombre moyen d'employés par commune dans le Nord ?
```sql
SELECT AVG(effectif) FROM evolution WHERE categorie = 'Employés';
```

### Extraction croisée

1. Affichez les catégories socioprofessionnelles, genres et effectifs
pour la ville de Caullery, sans utiliser le code postal, triées par effectifs croissants.
```sql
SELECT categorie, genre, effectif
  FROM ville JOIN evolution ON ville.code = evolution.code
  WHERE nom = 'Caullery'
  ORDER BY effectif;
```

2. Affichez les noms des villes, ainsi que les catégories et genres
des données ayant des effectifs à 0, triés par catégories.
```sql
SELECT nom, categorie, genre
  FROM ville JOIN evolution ON ville.code = evolution.code
  WHERE effectif = 0
  ORDER BY categorie;
```

3. Quelles sont les noms des villes ayant des catégories socioprofessionnelles dont les effectifs dépassent 2000 individus ?
```sql
SELECT nom
  FROM ville JOIN evolution ON ville.code = evolution.code
  WHERE effectif >= 2000;
```

4. Ordonnez le résultat par effectifs décroissants.
```sql
SELECT nom
  FROM ville JOIN evolution ON ville.code = evolution.code
  WHERE effectif >= 2000
  ORDER BY effectif;
```

5. Même question en supprimant la ville de Lille des résultats
possibles.
```sql
SELECT nom
  FROM ville JOIN evolution ON ville.code = evolution.code
  WHERE effectif >= 2000 AND nom != 'Lille'
  ORDER BY effectif;
```

### Modification des données

1. Insérez les données correspondants à une nouvelle ville de votre choix dans la table ville.
```sql
INSERT INTO ville VALUES('75000', '1', '75', 'Paris', '10.0,20.0');
```

2. Modifiez le nom de cette ville.
```sql
UPDATE ville SET nom = 'Hollywood' WHERE nom = 'Paris';
```

3. Finalement, supprimez-la.
```sql
DELETE FROM ville WHERE nom = 'Hollywood';
```

 ## Lundi 18 Novembre 2019

1. Connectez vous à la base de données `categories-socio-nord.db` depuis un programme.
Afficher les résultats des requêtes suivantes depuis ce programme :
- Afficher le code INSEE, les catégories socio-professionnelles, le genre et le nombre d'individus de la ville de 'Caullery'.
```python
# On se connecte
conn = sql.connect('categories-socio-nord.db')

# On crée un curseur
cur = conn.cursor()

cur.execute("SELECT ville.code, categorie, genre, effectif FROM ville JOIN evolution ON ville.code = evolution.code WHERE nom = 'Caullery';")
res = cur.fetchall()

for line in res:
    print(line)
```
- Afficher le nombre total de femmes dans le Nord.
```python
cur.execute("SELECT SUM(effectif) FROM ville JOIN evolution ON ville.code = evolution.code WHERE departement = 59 AND genre = 'Femmes';")
res = cur.fetchone()

nb_femmes = res[0]

print('Nombre de femmes dans le nord : {}.'.format(nb_femmes))
```

2. Depuis un programme, afficher le pourcentage de femmes dans le Nord.
Vous devez écrire une requête qui fait le calcul en utilisant la valeur obtenue précédemment.
```python
cur.execute("SELECT SUM(effectif) FROM ville JOIN evolution ON ville.code = evolution.code WHERE departement = 59;")
res = cur.fetchone()

print('Le pourcentage de femmes dans le nord est de {:.2f}%.'.format(nb_femmes*100/res[0]))
```
3. Afficher le code INSEE, le ou les codes postaux, le nom, les catégories socioprofessionnelles, les genres, les effectifs de la commune de Villeneuve-d'Ascq (jointure entre evolution et correspondance), sachant que cette commune est écrite ```VILLENEUVE-D'ASCQ``` dans la table `correspondance`.
```python
cur.execute('''SELECT codeINSEE, codePostal, commune, categorie, genre, effectif FROM correspondance
            JOIN evolution ON correspondance.codeINSEE = evolution.code
            WHERE commune = "VILLENEUVE-D'ASCQ";''')

res = cur.fetchall()

for line in res:
    print(line)
```

4. Depuis votre programme, écrivez deux versions d'une fonction qui renvoie une liste de tuples représentant
toutes les informations (code insee, le ou les codes postaux, nom,  catégories sociaux professionnelles, genre, effectif) sur la ville d'un code postal passé en paramètre.
```python
def get_informations_from_cp_a(cp):
    conn = sql.connect('categories-socio-nord.db')
    cur = conn.cursor()

    cur.execute('''SELECT codeINSEE, codePostal, commune, categorie, genre, effectif FROM correspondance
            JOIN evolution ON correspondance.codeINSEE = evolution.code WHERE INSTR(codePostal, {}) != 0;'''.format(cp))
    res = cur.fetchall()
    conn.close()
    return res

def get_informations_from_cp_b(cp):
    conn = sql.connect('categories-socio-nord.db')
    cur = conn.cursor()

    cur.execute('''SELECT codeINSEE, codePostal, commune, categorie, genre, effectif FROM correspondance
            JOIN evolution ON correspondance.codeINSEE = evolution.code;''')
    res = cur.fetchall()
    ville = []

    for line in res:
        if str(cp) in line[1]:
            ville.append(line)

    conn.close()
    return ville
```
