# Activité

## Les systèmes d'exploitation

### L'aspect historique

Un **système d'exploitation** (en anglais, *Operating System*) est un ensemble de programmes qui dirige l'utilisation des ressources d'un ordinateur.

On peut citer comme exemple :

|               MS-DOS                |                 UNIX                 |                 GNU/LINUX                 |
| :---------------------------------: | :----------------------------------: | :---------------------------------------: |
| <img src='img/MSDOS.jpg' width=200> | <img src="img/unix.png" width = 250> | <img src="img/gnu-linux.png" width = 300> |



> 1. Avec votre binôme, réalisez une présentation historique de l'un de ces systèmes d'exploitation.
>
>    Vous y exposerez un bref résumé historique de sa création ainsi que de son créateur.



### L'interpréteur de commande

Un **interpréteur de commandes** est un logiciel système faisant partie des composants de base d'un système d'exploitation. Sa fonction est d'interpréter les commandes qu'un utilisateur tape au clavier dans l'interface en ligne de commande.



> 1. Quel est le nom de l'interpréteur de commande initialement utilisé sous MS-DOS ?



Sous les systèmes de type Unix, il existe de nombreux interpréteurs de commandes qui dérivent essentiellement de **sh**.



![shell](img/sh.gif)



Pour connaître le type de shell utilisé sur votre système d'exploitation, vous pouvez taper la commande :

```
ps
```



## Travaux pratiques : l'interpréteur de commandes

### Prise en main

> 1. À l'aide de la commande `cd` , placez-vous à la racine de votre système.
>
> 2. À l'aide d'un chemin absolu, placez-vous dans votre dossier personnel `Documents` .
>
> 3. Créer l'arborescence suivante :
>
>    ![Arborescence](img/tree.png)
>
> 4. À l'aide de la commande `cp`, copier le répertoire `2019` dans le répertoire `2020` pour obtenir l'arborescence suivante :
>
>    ![Arborescence2](img/tree2.png)
>
> 5. Placez-vous dans le dosser `NSI/2019/Python/TPs`et créez-y un dossier `01` dans lequel vous créerez à l'aide de la commande `touch` un fichier nommé `hello_world.py`.
>
> 6. Éditez ce fichier avec l'éditeur `nano` et placez-y le code suivant :
>
>    ```python
>    print("Hello World.")
>    ```
>
> 7. Observer les droits du fichier `hello_world.py`. Est-il exécutable ?
>
> 8. Interpréter votre fichier à l'aide de la commande 
>
>    ```
>    python hello_world.py
>    ```
>
> 9. Dans le dossier `01`, créez le dossier `C` ainsi que le fichier `hello_world.c` contenant le code :
>
>    ```c
>    #include <stdio.h>
>    
>    int main()
>    {
>      printf("Hello World.\n");
>      return 0;
>    }
>    ```
>
> 10. Compiler ce fichier à l'aide de la commande `gcc hello_world.c -o hello_word` puis exécuter le fichier créé à l'aide de la commande `./hello_world`
>
> 11. Retirer les droits d'éxécution au fichier `hello_world` à l'aide de la commande `chmod` puis tenter à nouveau de l'éxécuter.
>
> 12. Comparer à travers cet exemple, la différence entre le langage python et le langage C.
